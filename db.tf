resource "helm_release" "mydatabase" {
  name  = var.database-name
  repository = "https://repo.iotblue.io/repository/helm-repo"
  repository_username = "admin"
  repository_password = "Iotblue55$"
  chart = "postgresql"
  

  set {
    name  = "postgresqlUsername"
    value = var.username
  }

  set {
    name  = "postgresqlPassword"
    value = var.password
  }

#   set {
#     type = "string"  
#     name  = "master.affinity.podAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].labelSelector.matchExpressions[0].key"
#     value = var.label-key
#   }

  set {
    # type = "string"  
    name  = "master.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].key"
    value = var.label-key
  }

#   set {
#     type = "string"  
#     name  = "master.affinity.podAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].labelSelector.matchExpressions[0].values[0]"
#     value = var.label-value
#   }

  set {
    # type = "string"  
    name  = "master.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].values[0]"
    value = var.label-value
  }       

#   set {
#     type = "string"  
#     name  = "slave.affinity.podAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].labelSelector.matchExpressions[0].key"
#     value = var.label-key
#   }

  set {
    # type = "string"  
    name  = "slave.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].key"
    value = var.label-key
  }

#   set {
#     type = "string"  
#     name  = "slave.affinity.podAffinity.requiredDuringSchedulingIgnoredDuringExecution[0].labelSelector.matchExpressions[0].values[0]"
#     value = var.label-value
#   }

  set {
    # type = "string"  
    name  = "slave.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].values[0]"
    value = var.label-value
  }      

  set {
    # type = "string"  
    name  = "master.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].operator"
    value = "In"
  }  

  set {
    # type = "string"  
    name  = "slave.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution.nodeSelectorTerms[0].matchExpressions[0].operator"
    value = "In"
  }  
}