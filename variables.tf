variable "username" {
  type = string
  description = "username"
}

variable "password" {
  type = string
  description = "password"
}

variable "label-key" {
  type = string
  description = "label-key"
}

variable "label-value" {
  type = string
  description = "label-value"
}

variable "database-name" {
  type = string
  description = "database-name"
}